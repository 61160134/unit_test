package UnitTest;

import static org.junit.jupiter.api.Assertions.*;
import UnitTest.Table;
import UnitTest.Player;

import org.junit.jupiter.api.Test;

class TableUnitTest {

	@Test
	void Row1() {
		Player o = new Player('o');
		Player x = new Player('x');
		Table table = new Table(o, x);
		table.setRowCol(1, 1);
		table.setRowCol(1, 2);
		table.setRowCol(1, 3);
		assertEquals(true,table.checkWin());
	}
	void Row2() {
		Player o = new Player('o');
		Player x = new Player('x');
		Table table = new Table(o, x);
		table.setRowCol(2, 1);
		table.setRowCol(2, 2);
		table.setRowCol(2, 3);
		assertEquals(true,table.checkWin());
	}
	void Row3() {
		Player o = new Player('o');
		Player x = new Player('x');
		Table table = new Table(o, x);
		table.setRowCol(3, 1);
		table.setRowCol(3, 2);
		table.setRowCol(3, 3);
		assertEquals(true,table.checkWin());
	}
	void Col1() {
		Player o = new Player('o');
		Player x = new Player('x');
		Table table = new Table(o, x);
		table.setRowCol(1, 1);
		table.setRowCol(2, 1);
		table.setRowCol(3, 1);
		assertEquals(true,table.checkWin());
	}
	void Col2() {
		Player o = new Player('o');
		Player x = new Player('x');
		Table table = new Table(o, x);
		table.setRowCol(1, 2);
		table.setRowCol(2, 2);
		table.setRowCol(3, 2);
		assertEquals(true,table.checkWin());
	}
	void Col3() {
		Player o = new Player('o');
		Player x = new Player('x');
		Table table = new Table(o, x);
		table.setRowCol(1, 3);
		table.setRowCol(2, 3);
		table.setRowCol(3, 3);
		assertEquals(true,table.checkWin());
	}
	void X1() {
		Player o = new Player('o');
		Player x = new Player('x');
		Table table = new Table(o, x);
		table.setRowCol(1, 1);
		table.setRowCol(2, 2);
		table.setRowCol(3, 3);
		assertEquals(true,table.checkWin());
	}
	void X2() {
		Player o = new Player('o');
		Player x = new Player('x');
		Table table = new Table(o, x);
		table.setRowCol(1, 3);
		table.setRowCol(2, 2);
		table.setRowCol(3, 1);
		assertEquals(true,table.checkWin());
	}
	void swt() {
		Player o = new Player('o');
		Player x = new Player('x');
		Table table = new Table(o, x);
		table.switchPlayer();
		assertEquals('x',table.getCurrentPlayer().getName());
	}
	void draw() {
		Player o = new Player('o');
		Player x = new Player('x');
		Table table = new Table(o, x);
		table.countTurn = 9;		
		assertEquals(true,table.checkWin());
	}








}
